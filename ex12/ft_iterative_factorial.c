/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 12:23:41 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/11/14 20:10:17 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int res;

	res = nb;
	if ((nb == 1) || (nb == 0))
		return (1);
	if ((nb > 0) && (nb <= 12))
	{
		while (nb > 1)
		{
			res = res * (nb - 1);
			nb--;
		}
		return (res);
	}
	return (0);
}
